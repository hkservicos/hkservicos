'use strict';

angular.module('myApp.trabalhe', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
 
}])

.controller('TrabalheCtrl', [ '$scope', '$location', '$http', 'vcRecaptchaService', function($scope, $location, $http, vcRecaptchaService) {
	
	$scope.empregado = {};
	$scope.valor = {};
	$scope.numeracao = {};
	var d;
	
		var vm = this;
			vm.publicKey = "6LdFPiYTAAAAAG-kVScDTAe5b_WTIdnDp23-DM_e"
	
		$scope.enviarEmail = function(){
			$scope.emailEnviado = true;
		}
		
		var param = function(data) {
	 		var returnString = '';
	 		for (d in data){
	 			if (data.hasOwnProperty(d))
	 				returnString += d + '=' + data[d] + '&';
	 		}
	 		// Remove o último & que não é necessário
	 		return returnString.slice( 0, returnString.length - 1 );
	 	};
	    
	 	vm.enviarEmail = function(){
	
	     /* vcRecaptchaService.getResponse() gives you the g-captcha-response */
	
	        if(vcRecaptchaService.getResponse() === ""){ //if string is empty
	            alert("Por favor resolva o captcha")
	        }else {
	        	
	        	//Enviar Email formulario de contato
	        	
	            var formData = {  //prepare payload for request
	                'nome':vm.nome,
	                'email':vm.email,
	                'assunto':vm.assunto,
	                'msg':vm.mensagem,
	                'g-recaptcha-response':vcRecaptchaService.getResponse()  //send g-captcah-response to our server
	            }
	
	            $http({
					method : 'POST',
					url : 'email.php',
					data : param(formData),
					headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
				})
				//função de callback quando a promise for sucesso
				.success(function(data) {
					//verifica o retorno do servidor se o email foi enviado
					if (data.enviado) {
						alert("Email enviado com sucesso, obrigado!");
					   $scope.emailEnviado = true; //ocultamos o formulário e exibimos mensagem de sucesso
					} else {
					  	$scope.emailEnviado = false;
					}
				})
				//função de callback quando a promise for erro (geralmente problema de conexão ou página não existente)
				.error(function(error){
					$scope.emailEnviado = true;
					alert("Desculpe, houve um problema ao enviar sua mensagem, tente novamente mais tarde.");
				});
	            /* MAKE AJAX REQUEST to our server with g-captcha-string 
	                $http.post('http://sitename.com/api/signup',formData).success(function(response){
	                if(response.error === 0){
	                    alert("Successfully verified and signed up the user");
	                }else{
	                    alert("User verification failed");
	                }
	            })
	            .error(function(error){
	
	            })*/
	        }
	    }
	
	
    $scope.formataValorAuxilio = function(){
    	$scope.valor.auxilioTransporte = $scope.formataValor($scope.valor.auxilioTransporte);
    };
    
    $scope.formataValorSalario = function(){
    	$scope.valor.salario = $scope.formataValor($scope.valor.salario);
    };
	
	$scope.formataValor = function(valor){
		if(valor != "" && valor != undefined){
			valor = String(valor);
			valor = valor.replace(/\D/g,'');
			if(valor.length >= 3){
				valor = valor.substring(0,valor.length - 2) +","+ valor.substring(valor.length - 2);
				valor = valor.replace(/^0+(?!\,|$)/, '');
			}else if(valor.length == 2){
				valor = "0,"+valor;	
			}else{
				valor = "0,0"+valor;
			}
		}
		return ("R$" + valor);
	};
	
	$scope.myDate = new Date();
	$scope.minDate = new Date($scope.myDate.getFullYear(), $scope.myDate.getMonth() - 12, $scope.myDate.getDate());
	$scope.maxDate = new Date($scope.myDate.getFullYear(), $scope.myDate.getMonth() + 1, $scope.myDate.getDate());

	$scope.onlyWeekendsPredicate = function(date) {
	var day = date.getDay();
	return day === 0 || day === 6;
	}
	
	$scope.exibirValor = function(){
		return $scope.optaAuxilio;
	};
	
	$scope.exibirNumeracao = function(){
		return $scope.usaUniforme;
	};
	
	$scope.exibirReservista = function(){
		if($scope.empregado.sexo == "m"){
		return true;
		};
	};
	
	$scope.exibirDocumentos = function(){
		return $scope.termoAceito;
	};
	
	$scope.exibeCertidao = function(){
		
		return $scope.empregado.estadoCivil == 'solteiro' || $scope.empregado.estadoCivil == null ? true : false;
			
	};
	
	$scope.concluirCadastro = function(){
		console.log($scope.empregado)
	};
}]);