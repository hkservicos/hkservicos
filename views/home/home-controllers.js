'use strict';

angular.module('myApp.home', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
 
}])

.controller('HomeCtrl', [ '$scope', '$location', '$http', 'vcRecaptchaService', function($scope, $location, $http, vcRecaptchaService) {
	
	$scope.voltar = [];
	$scope.emailEnviado = false;
	$scope.trabalheConosco = false;
	var d;
	
	 var vm = this;
 		vm.publicKey = "6LdFPiYTAAAAAG-kVScDTAe5b_WTIdnDp23-DM_e"
	 	
 			var param = function(data) {
	 		var returnString = '';
	 		for (d in data){
	 			if (data.hasOwnProperty(d))
	 				returnString += d + '=' + data[d] + '&';
	 		}
	 		// Remove o último & que não é necessário
	 		return returnString.slice( 0, returnString.length - 1 );
	 	};
	    
	 	vm.enviarEmail = function(){

	     /* vcRecaptchaService.getResponse() gives you the g-captcha-response */

	        if(vcRecaptchaService.getResponse() === ""){ //if string is empty
	            alert("Por favor resolva o captcha")
	        }else {
	        	
	        	//Enviar Email formulario de contato
	        	
	            var formData = {  //prepare payload for request
	                'nome':vm.nome,
	                'email':vm.email,
	                'assunto':vm.assunto,
	                'mensagem':vm.mensagem,
	                'g-recaptcha-response':vcRecaptchaService.getResponse()  //send g-captcah-response to our server
	            }

	            $http({
					method : 'POST',
					url : 'email.php',
					data : param(formData),
					headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
				})
				//função de callback quando a promise for sucesso
				.success(function(data) {
					//verifica o retorno do servidor se o email foi enviado
					if (data.enviado) {
						alert("Email enviado com sucesso, obrigado!");
					   $scope.emailEnviado = true; //ocultamos o formulário e exibimos mensagem de sucesso
					} else {
					  	$scope.emailEnviado = false;
					}
				})
				//função de callback quando a promise for erro (geralmente problema de conexão ou página não existente)
				.error(function(error){
					$scope.emailEnviado = true;
					alert("Desculpe, houve um problema ao enviar sua mensagem, tente novamente mais tarde.");
				});
	            /* MAKE AJAX REQUEST to our server with g-captcha-string 
	                $http.post('http://sitename.com/api/signup',formData).success(function(response){
	                if(response.error === 0){
	                    alert("Successfully verified and signed up the user");
	                }else{
	                    alert("User verification failed");
	                }
	            })
	            .error(function(error){

	            })*/
	        }
	    }
	
	var calcularIdade = function(){
		
		var criacao	 = new Date('2013').getFullYear();
		var data 	 = new Date().getFullYear();
		$scope.idade = data - criacao;
	}

	$scope.init = function(){
		
		calcularIdade();
	};
	
	$scope.servicos = function(){
		$location.path('/servicos')
	}
	
	$scope.trabalhe = function(){
		$location.path('/trabalhe')
		$scope.trabalheConosco = true;
	}
	$scope.ocultarMenu = function(){
		return $scope.trabalheConosco;
	}
	
	$scope.voltar = function(){
		$location.path('/home');
	}
	

	
}]);
