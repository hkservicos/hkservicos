'use strict';

angular.module('myApp.solucoes', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  
}])

.controller('SolucoesCtrl', ['$scope', '$location', '$http', '$window', '$anchorScroll', 'vcRecaptchaService' , function($scope, $location, $http, $window, $anchorScroll, vcRecaptchaService) {
	
	$scope.emailEnviado = false;
	var d;
	var vm = this;
	vm.publicKey = "6LdFPiYTAAAAAG-kVScDTAe5b_WTIdnDp23-DM_e"
	var param = function(data) {
        var returnString = '';
        for (d in data){
            if (data.hasOwnProperty(d))
               returnString += d + '=' + data[d] + '&';
        }
        // Remove o último & que não é necessário
        return returnString.slice( 0, returnString.length - 1 );
	};
	
	$scope.init = function(){
		$(document).ready(function(){
			$(this).scrollTop(850);
		});
	}
	
	$scope.contratar = function(){
		
		$location.hash('contact-section');
		$anchorScroll();
		$scope.formData.assunto = "hospedagem";
        
	}
	
	vm.enviarEmail = function(){
		
		if(vcRecaptchaService.getResponse() === ""){ //if string is empty
            alert("Por favor resolva o captcha")
        }else {
		
        	//Enviar Email formulario de contato
        	
            var formData = {  //prepare payload for request
                'nome':vm.nome,
                'email':vm.email,
                'assunto':vm.assunto,
                'msg':vm.mensagem,
                'g-recaptcha-response':vcRecaptchaService.getResponse()  //send g-captcah-response to our server
            }
            console.log(formData);
		
			$http({
				method : 'POST',
				url : 'email.php',
				data : param(formData),
				headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
			})
			//função de callback quando a promise for sucesso
			.success(function(data) {
				//verifica o retorno do servidor se o email foi enviado
				if (data.enviado) {
					alert("Email enviado com sucesso, obrigado!");
				   vm.emailEnviado = true; //ocultamos o formulário e exibimos mensagem de sucesso
				} else {
				  	vm.emailEnviado = false;
				}
			})
			//função de callback quando a promise for erro (geralmente problema de conexão ou página não existente)
			.error(function(error){
				vm.emailEnviado = true;
				alert("Desculpe, houve um problema ao enviar sua mensagem, tente novamente mais tarde.");
			});
		}
	}
	
}]);