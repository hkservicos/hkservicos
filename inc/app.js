'use strict';

// Declare app level module which depends on views, and components
angular.module('store', [
  'ngRoute',
  'myApp.home',
  'myApp.servicos',
  'myApp.solucoes',
  'myApp.trabalhe',
  'vcRecaptcha'
]).
//Verificar uso(Formulário de trabalhe conosco)
directive('myDirective', function (httpPostFactory) {
    return {
        restrict: 'A',
        scope: true,
        link: function (scope, element, attr) {

            element.bind('change', function () {
                var formData = new FormData();
                formData.append('file', element[0].files[0]);
                httpPostFactory('upload_image.php', formData, function (callback) {
                   // recieve image name to use in a ng-src 
                    console.log(callback);
                });
            });

        }
    };
}).
//Verificar uso(Formulário de trabalhe conosco)
factory('httpPostFactory', function ($http) {
    return function (file, data, callback) {
        $http({
            url: file,
            method: "POST",
            data: data,
            headers: {'Content-Type': undefined}
        }).success(function (response) {
            callback(response);
        });
    };
}).

config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
	
	$routeProvider
	.when('/home', {
      templateUrl: 'views/home/home.html',
      controller: 'HomeCtrl',
      controllerAs: 'home'
	})
	.when('/servicos', {
      templateUrl: 'views/servicos/servicos.html',
      controller: 'ServicosCtrl',
      controllerAs: 'servicos'
    })
    .when('/solucoes', {
      templateUrl: 'views/solucoes/solucoes.html',
      controller: 'SolucoesCtrl',
      controllerAs: 'solucoes'
    })
	.when('/trabalhe', {
	      templateUrl: 'views/trabalhe/trabalhe.html',
	      controller: 'TrabalheCtrl',
	      controllerAs: 'trabalhe'
	});
	
	$routeProvider.otherwise({redirectTo: '/home'});
}]);
